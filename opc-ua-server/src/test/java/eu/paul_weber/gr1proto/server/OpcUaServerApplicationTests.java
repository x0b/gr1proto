package eu.paul_weber.gr1proto.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpcUaServerApplicationTests {

    @Test
    public void contextLoads() {
    }

}
