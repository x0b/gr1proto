package eu.paul_weber.gr1proto.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpcUaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpcUaServerApplication.class, args);
    }
}
