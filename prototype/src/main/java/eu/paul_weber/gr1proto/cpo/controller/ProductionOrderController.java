/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.controller;

import eu.paul_weber.gr1proto.cpo.model.*;
import eu.paul_weber.gr1proto.cpo.service.ProductionOrderService;
import eu.paul_weber.gr1proto.cpo.service.ServiceProviderAvailabilityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v-proto/production")
public class ProductionOrderController {

    private Logger log = LoggerFactory.getLogger(ProductionOrderController.class);
    private ProductionOrderService productionOrderService;

    @Autowired
    public ProductionOrderController(ProductionOrderService productionOrderService) {
        this.productionOrderService = productionOrderService;
    }

    @GetMapping(value = "/grades", params = {"dropdown"})
    public SuccessResponse<List<DropdownResponse>> getProductionGrades(){
        return new SuccessResponse<>(
                Arrays.stream(ProductionGrade.values()).map(EnumDropdown::new).collect(Collectors.toList())    ,
                true);
    }

    @PostMapping("/order")
    public ResponseEntity<ProductionOrder> createProductionOrder(@RequestBody ProductionOrder productionOrder){
        try {
            ProductionOrder order = productionOrderService.createProductionOrder(productionOrder);
            return new ResponseEntity<>(order, HttpStatus.CREATED);
        } catch (ServiceProviderAvailabilityException e) {
            log.error("Order could not be created because the service failed");
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

}
