/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.service;

public class ServiceProviderAvailabilityException extends RuntimeException {

    private static final String message = "Service provider unavailable";

    public ServiceProviderAvailabilityException() {
        super(message);
    }
}
