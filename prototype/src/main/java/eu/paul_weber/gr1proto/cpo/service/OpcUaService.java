/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.service;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.UaClient;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfig;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfigBuilder;
import org.eclipse.milo.opcua.stack.client.UaTcpStackClient;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.springframework.stereotype.Service;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

//@Service
public class OpcUaService {

    /**
     * Timeout for retrieving endpoints in seconds
     */
    public static final long ENDPOINT_TIMEOUT = 10;
    private boolean haveEndpoints = false;
    private UaClient client;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public OpcUaService(String url){
        haveEndpoints = tryConnect(url);
    }

    /**
     * Try to connect to the endpoints available under the url
     * @param url
     * @return
     */
    public boolean tryConnect(String url){
        try {
            EndpointDescription[] endpointDescriptions = UaTcpStackClient.getEndpoints(url).get(ENDPOINT_TIMEOUT, TimeUnit.SECONDS);
            OpcUaClientConfig config = new OpcUaClientConfigBuilder()
                                            .setEndpoint(endpointDescriptions[0])
                                            .build();
            OpcUaClient client = new OpcUaClient(config);
            this.client = client.connect().get(ENDPOINT_TIMEOUT, TimeUnit.SECONDS);
            return true;
        } catch (TimeoutException | ExecutionException | InterruptedException e) {
            return false;
        }
    }

    public boolean hasEndpoints(){
        return haveEndpoints;
    }
}
