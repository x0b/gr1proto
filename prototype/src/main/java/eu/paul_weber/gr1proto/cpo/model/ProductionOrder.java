/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductionOrder {
    private long id;
    private Product product;
    private ProductColor color;
    private long quantity;
    private ProductionGrade grade;

    @JsonCreator
    public ProductionOrder(@JsonProperty("id") long id,
                           @JsonProperty("product") Product product,
                           @JsonProperty("color") ProductColor color,
                           @JsonProperty("quantity") long quantity,
                           @JsonProperty("grade") ProductionGrade grade) {
        this.id = id;
        this.product = product;
        this.color = color;
        this.quantity = quantity;
        this.grade = grade;
    }

    public long getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public ProductColor getColor() {
        return color;
    }

    public long getQuantity() {
        return quantity;
    }

    public ProductionGrade getGrade() {
        return grade;
    }

    /**
     * Assign an id to the ProductionOrder. If it already has one, this _will_ throw IllegalArgumentException
     * @param id
     */
    public void assignId(long id){
        if(id < 0){
            this.id = id;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
