/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.model;

public interface DropdownResponse {

    String getName();
    String getValue();
}
