/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */

package eu.paul_weber.gr1proto.cpo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CreateProductionOrderApplication extends SpringBootServletInitializer {

    /**
     * Initializer calls for war deployment
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(CreateProductionOrderApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(CreateProductionOrderApplication.class, args);
    }
}
