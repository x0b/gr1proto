/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.model;

public class SuccessResponse<T> {
    private T payload;
    private boolean successfull;

    public SuccessResponse(T payload, boolean successfull) {
        this.payload = payload;
        this.successfull = successfull;
    }

    public boolean getSuccess(){
        return successfull;
    }

    public T getResults(){
        return payload;
    }
}
