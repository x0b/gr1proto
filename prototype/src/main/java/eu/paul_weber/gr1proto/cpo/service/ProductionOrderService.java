/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.service;

import eu.paul_weber.gr1proto.cpo.model.Product;
import eu.paul_weber.gr1proto.cpo.model.ProductionOrder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ProductionOrderService {

    /**
     * This is where this prototype cheats: it does not send any orders, it only stores them
     */
    private List<ProductionOrder> orderCache;
    /**
     * Order cache index
     */
    private int oci = 0;

    public ProductionOrderService() {
        // this is a high performance server - or at least we shouldn't shoot ourselves in the foot just yet :-)
        this.orderCache = Collections.synchronizedList(new ArrayList<>());
    }

    /**
     * Process a production order and forward it to all subscribing upstream servers
     * @param productionOrder
     * @return
     */
    public ProductionOrder createProductionOrder(ProductionOrder productionOrder){
        return sendProductionOrder(productionOrder);
    }

    private ProductionOrder sendProductionOrder(ProductionOrder productionOrder){
        if(productionOrder.getId() < 0)
            productionOrder.assignId(oci++);
        orderCache.add(productionOrder);
        return productionOrder;
    }
}
