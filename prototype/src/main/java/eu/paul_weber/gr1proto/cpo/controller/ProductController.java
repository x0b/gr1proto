/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.controller;

import eu.paul_weber.gr1proto.cpo.model.DropdownResponse;
import eu.paul_weber.gr1proto.cpo.model.Product;
import eu.paul_weber.gr1proto.cpo.model.ProductColor;
import eu.paul_weber.gr1proto.cpo.model.SuccessResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v-proto/product")
public class ProductController {

    @GetMapping("/colors")
    public List<ProductColor> getColors(){
        return Arrays.asList(ProductColor.values());
    }

    @GetMapping(value = "/colors", params = "dropdown")
    public SuccessResponse<List<DropdownResponse>> getColorsAsDropdown(){
        return new SuccessResponse<>(Arrays.stream(ProductColor.values()).map(c -> new DropdownResponse() {
            @Override
            public String getName() {
                return c.name();
            }

            @Override
            public String getValue() {
                return Integer.toString(c.ordinal());
            }
        }).collect(Collectors.toList()), true);
    }

    @GetMapping
    public List<Product> getProducts(){
        return Arrays.asList(
                new Product(1, "Product #1"),
                new Product(2, "Product #2"),
                new Product(3, "Product #3"),
                new Product(4, "Product #4"),
                new Product(5, "Product #5"));
    }

    @GetMapping(params = {"dropdown"})
    public SuccessResponse<List<ProductDropdown>> getProductsAsDropdown(){
        return new SuccessResponse<>(
                getProducts().stream().map(ProductDropdown::new).collect(Collectors.toList()), true
        );
    }

    public SuccessResponse<List<DropdownResponse>> dd2(){
        List<DropdownResponse> list = new ArrayList<>();
        for (Product p : getProducts()) {
            DropdownResponse dropdownResponse = new DropdownResponse() {
                @Override
                public String getName() {
                    return p.getName();
                }

                @Override
                public String getValue() {
                    return Long.toString(p.getId());
                }
            };
            list.add(dropdownResponse);
        }
        return new SuccessResponse<>(list, true);
    }

    public interface DropdownItem {

        public String getName();

        public String getValue();
    }

    public static class ProductDropdown implements DropdownItem {

        private Product product;

        public ProductDropdown(Product product) {
            this.product = product;
        }

        @Override
        public String getName() {
            return product.getName();
        }

        @Override
        public String getValue() {
            return Long.toString(product.getId());
        }
    }
}
