/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */
package eu.paul_weber.gr1proto.cpo.model;

public class EnumDropdown<T extends Enum<T>> implements DropdownResponse {

    private T payload;

    public EnumDropdown(T payload) {
        this.payload = payload;
    }

    @Override
    public String getName() {
        return payload.name();
    }

    @Override
    public String getValue() {
        return Integer.toString(payload.ordinal());
    }
}
