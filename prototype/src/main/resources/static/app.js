/*
 * Copyright (c) 2018. Paul Weber
 * ALL RIGHTS RESERVED. SEE LICENSING.MD.
 */

// shc
var query = document.querySelector.bind(document);
var queryAll = document.querySelectorAll.bind(document);
var fromId = document.getElementById.bind(document);
var fromClass = document.getElementsByClassName.bind(document);
var fromTag = document.getElementsByTagName.bind(document);

/**
 * Execute a listener function when the element if the element is activated
 * via click, enter or space bar
 * @param query CSS query to get the element or an element to attach to
 * @param fn a function that is executed when the event is fired
 */
function onActivateElement(query, fn) {
    var element = null;
    if (query instanceof EventTarget) {
        element = query;
    } else {
        element = document.querySelector(query);
    }
    if (element) {
        element.addEventListener('click', fn);
        element.addEventListener('keydown', function (event) {
            if (event.key === 'Enter' || event.key === ' ') {
                fn.call(element, event);
            }
        });
    } else {
        console.warn("EventListener binding failed, selector ``" + query + "´´");
    }
}

// configure service api
$.fn.api.settings.api = {
    'get product names': '/products/names',
    'create product order': '/productorder'
};

// init elems
$('.ui.dropdown').dropdown();

$('.ui.dropdown.v-material').dropdown({
    filterRemoteData: true,
    placeholder: 'Material eingeben',
    //fields: {
    //    remoteValues: 'techs',
    //    name: "name",
    //    value: "id"
    //},
    apiSettings: {
        url: 'api/v-proto/product?dropdown',
        method: 'get',
        // onResponse: function (r) {
        //     var response = {
        //         results: []
        //     };
        //     if (!r) {
        //         return;
        //     }
        //     for (var i = 0; i < r.length; i++) {
        //         response.results.push({name: r[i].name, value: r[i].id});
        //     }
        //
        //     return response;
        // },
        // onResponse: function(response){
        //     return {
        //         success: true,
        //         results: response
        //     };
        // },
        filterRemoteData: true,
        saveRemoteData: false
    }
});

$('.ui.dropdown.v-color').dropdown({
    placeholder: 'Farbe eingeben',
    apiSettings: {
        url: 'api/v-proto/product/colors?dropdown',
        method: 'get',
        filterRemoteData: true,
        saveRemoteData: false
    }
});
$('.ui.dropdown.v-grade').dropdown({
    placeholder: 'Farbe eingeben',
    apiSettings: {
        url: 'api/v-proto/production/grades?dropdown',
        method: 'get',
        filterRemoteData: true,
        saveRemoteData: false
    }
});

// enable date pickers
$('#rangestart').calendar({
    type: 'date',
    endCalendar: $('#rangeend')
});
$('#rangeend').calendar({
    type: 'date',
    startCalendar: $('#rangestart')
});

/*
$('#btnCreateOrder').on('click', function(env) {
    console.log(env);
    console.log(this);
});
document.getElementById('btnCreateOrder').addEventListener('click', function(element){
    console.log(element);
    console.log(this);
});*/

/**
 * Pack the values of all contained children's form input elments into an object.
 * Function will scan the children for any valid element (meaning, id needs a name).
 * formBind for Jackson (c) 2017-2018 Paul Weber, licensed under MIT/X-License
 * @param form
 */
function formData(form) {
    var formTarget = null;
    if (form instanceof Element) {
        formTarget = form;
    } else {
        formTarget = document.querySelector(form);
    }

    var elements = formTarget.getElementsByTagName('input')
    //var textareas = Array.prototype.slice.call(formTarget.getElementsByTagName('textarea'));
    //elements.push.apply(elements, textareas);

    var data = {};
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        if (!element.hasAttribute('name')) {
            continue;
        }
        // serialize as string
        if (element.type === 'text'
            || element.type === 'hidden'
            || element.type === 'password'
            || (element.type === 'checkbox' && element.value)) {
            data[element.getAttribute('name')] = element.value;
        } else if (element.type === 'checkbox') {
            data[element.getAttribute('name')] = !!element.checked;
        } else if (element.type === 'radio' && element.checked) {
            data[element.getAttribute('name')] = !!element.checked;
        }
    }

    var textareas = formTarget.getElementsByTagName('textarea');
    for (var i = 0; i < textareas.length; i++) {
        var textarea = textareas[i];
        if (!textarea.hasAttribute('name')) {
            continue;
        }
        data[textarea.getAttribute('name')] = textarea.value;
    }

    // selects are either flattened or packed as arrays (for multiple)
    var selects = form.getElementsByTagName('select');
    for (var i = 0; i < selects.length; i++) {
        var select = selects[i];
        if (!select.hasAttribute('name') || select.selectedIndex === -1) {
            continue;
        }
        if (select.hasAttribute('multiple')) {
            var selectValues = [];
            for (var j = 0; j < select.options.length; j++) {
                var option = select.options[j];
                if (option.selected) {
                    selectValues.push(option.value || option.text);
                }
            }
            data[select.getAttribute('name')] = selectValues;
        } else {
            var option = select.options[select.selectedIndex];
            data[select.getAttribute('name')] = option.value || option.text;
        }
    }

    //console.log(data);
    return data;
}

// convert if directed to, returns unchanged if not supported
function applyExtraTyping(value, element){
    if(!element.hasAttribute('data-type')){
        return;
    }
    var targetType = element.getAttribute('data-type');
    var actualType = typeof value;
    if(targetType === actualType){
        return;
    }
    if(actualType === 'string'){
        switch(targetType){
            case 'number':
                return +value;
            case 'boolean':
                return value === 'true' || value === '1' || false;
            case 'object':
                try {
                    return JSON.parse(value);
                } catch(e){
                    return new String(value);
                }
            case 'array':
                return value.split(',').map(Function.prototype.call, String.prototype.trim);
        }
    }
    return value;
}

onActivateElement('#btnCreateOrder', function (event) {
    var data = formData(fromId('formCreateOrder'));
    console.log(data);
        var req = new XMLHttpRequest();
        req.addEventListener('load', function(event){
            if(req.status === 200) {
                var response = JSON.parse(req.responseText);
                console.log(response);
            }
        });
        req.addEventListener('error', function (event) {
            console.log('Error when posting trade');
            console.log(event);
        });
        req.open('post', '/api/v-proto/production/order', true);
        req.setRequestHeader('Accept', 'application/json');
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify(form));

});
