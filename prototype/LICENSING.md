Copyright (c) 2018 Paul Weber

## Limited license grant
As an exception to the above copyright notice, some rights are granted under the following circumstances:
1. Any rights granted only apply for the following projects and people working on them:
   - ```MW 350 Praxisprojekt IoT```
2. Any rights granted are only valid for the duration of the  project:
   - ```25.09.2018 - 18.12.2018```
3. The rights granted within this project scope are
   - **USE**: The right to download, install, configure or run a copy of this software source code, documentation or binaries, 
   - **MODIFY:** The right to change or otherwise adapt the software, including incorporating parts of the software into other software within the project scope. 
4. These rights are granted free of charge
5. These rights are only valid as long as any copyright notices within the software are preserved.
## Warranties and Liabilities
THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS´´ AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.