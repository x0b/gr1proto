# MW340 Group 1 Application Prototype
<div style="border: 1 px solid red; padding: 5px">
	<b>Note:</b> This application ist kept here for preservation, not for development. Any issues and pull requests will be ignored. 
</div>

This prototype was developed by me as part of the curriculum in the project module MW350 at the Ludwigshafen University of Applied Sciences.

On the techinal side, a web application using Spring Boot with an embedded Servlet Container was developed with the capability of dual war/jar packaging. 

The later application was supposed to connect to a OPC-UA Server to transmit production orders, but this goal was never realised due to technical problems.